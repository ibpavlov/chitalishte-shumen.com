This Repository is moved to Github: [https://github.com/ibpavlov/chitalishte-shumen](https://github.com/ibpavlov/chitalishte-shumen)

# Читалища Регион Шумен (Читалище Шумен) #

Това е един проект направен по време на [Code4.Tech](http://code4.tech) в [Шумен](http://code4.tech/shumen) състезание 2016 г. Цели подготвяне на ученици, студенти и професионалисти за интервю. 
С тази платформа може да получите информация за читалищата в област Шумен, да допринесете с информация за събития, новини, снимки и да бъдете част от една локална общност.

### Автори ###

* Ивелин Павлов
* Данаил Карадалиев
* Симеон Бойчев

### За кого е хранилището? ###

* За програмисти
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Преди да започнем? ###

* PHP 5.6+ 
* Phalcon 3+
* Mysql server

### Лиценз ###

* Свободен за ползване - MIT

### Как да го инсталираме ###

1. Трябва да нагласите сървъра ви да сочи към главната папка на проекта с rewrite access, и да е основен домейн. Можете директно да използвате [PHP Build in Server](http://php.net/manual/en/features.commandline.webserver.php)  
2. Да качите базата данни от папка /scheme където желаете - localhost или remote
3. Да създадете копие на файла /app/config/config.dist.ini и вече може да започнете
4. Готово, може да тествате дали работи